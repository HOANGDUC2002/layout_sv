 function layThongTinTuForm(){
    var ma=document.getElementById("txtMaSV").value.trim();
    var ten=document.getElementById("txtTenSV").value.trim();
    var email=document.getElementById("txtEmail").value.trim();
    var matKhau=document.getElementById("txtPass").value.trim();
    var diemToan=document.getElementById("txtDiemToan").value.trim();
    var diemLy=document.getElementById("txtDiemLy").value.trim();
    var diemHoa=document.getElementById("txtDiemHoa").value.trim();
    var sv=new SinhVien(ma,ten,email,matKhau,diemToan,diemLy,diemHoa);

    return sv;
 }


 function renderDSSV(list){
    var contentHTML="";
    for (var i=0; i<list.length; i++){
        var currenSV=list[i];
        var contenTr=`<tr>
        <td>${currenSV.ma}</td>
        <td>${currenSV.ten}</td>
        <td>${currenSV.email}</td>
        <td>0</td>
        <td> <button onclick="xoaSV('${currenSV.ma}')" class="btn btn-danger">xóa</button>
        <button onclick="suaSV('${currenSV.ma}')" class="btn btn-primary">sửa</button></td>
        </tr>`;
        contentHTML += contenTr;
    }
    document.getElementById("tbodySinhVien").innerHTML=contentHTML;
}
function showThongTinLenForm(sv){
    document.getElementById("txtMaSV").value=sv.ma;
    document.getElementById("txtTenSV").value=sv.ten;
    document.getElementById("txtEmail").value=sv.email;   
    document.getElementById("txtPass").value=sv.matKhau;
    document.getElementById("txtDiemToan").value=sv.diemToan;
    document.getElementById("txtDiemLy").value=sv.diemLy;
    document.getElementById("txtDiemHoa").value=sv.diemHoa;
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}
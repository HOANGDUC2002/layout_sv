var dssv = [];
var dssvJson = localStorage.getItem("DSSV");
if (dssvJson) {

    dssv = JSON.parse(dssvJson);
    renderDSSV(dssv);
}
function saveLocalStorage() {
    dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dssvJson);
}
function themSV() {
    var newSV = layThongTinTuForm();
    dssv.push(newSV);
    saveLocalStorage();
    renderDSSV(dssv);

    renderDSSV(dssv);
    resetForm();
}

function xoaSV(idSV){
   var index=dssv.findIndex(function(sv){
       return sv.ma==idSV;
    });
    if(index==-1) return;
    dssv.splice(index, 1);
    saveLocalStorage();
    renderDSSV(dssv); 
}

function suaSV(idSV) {
    var index=dssv.findIndex(function(sv){
        return sv.ma==idSV;
    });
    if(index==-1) return;
    var sv=dssv[index];
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disabled = true;
}
function capNhat(){
    var svEdit=layThongTinTuForm();
    var index = dssv.findIndex(function (sv) {
        return sv.ma == svEdit.ma;
      });
      if (index == -1) return;
  dssv[index] = svEdit;
  renderDSSV(dssv);
  resetForm();
  saveLocalStorage();
  document.getElementById("txtMaSV").disabled = false;

}
